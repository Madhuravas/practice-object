const school = {
    "engineering": {
        "students": 324,
        "dep_id": 3,
        "faculties": 4
    },
    "medical": {
        "students": 499,
        "dep_id": 2,
        "faculties": 6
    },
    "pure-science": {
        "students": 133,
        "dep_id": 1,
        "faculties": 2
    },
    "linguistics": {
        "students": 183,
        "dep_id": 4,
        "faculties": 3
    },
    "philosophy": {
        "students": 73,
        "dep_id": 5,
        "faculties": 2
    }
}

/*
 
    1. Group the data in terms of deparment id.
    2. Filter data with students greater than 200.
    3. Add a new property (labs-required) to each object . Set value as true for engineering medical and purescience ..and false for lingustics and philosophy
 
*/

// Question 1 

let groupTheDataByDept = (school) => {
    const changeSchoolData = JSON.parse(JSON.stringify(school))
    const result = Object.keys(changeSchoolData).reduce((obj,key) => {
        const dept_id = "dept_id" + changeSchoolData[key].dep_id
        let eachDept = Object.assign(changeSchoolData[key])
        eachDept.department = key
        delete eachDept.dep_id
        obj[dept_id] = eachDept
        return obj
    },{})
    console.log(result)
}

groupTheDataByDept(school)
console.log(school);

//Question 2 


let filterData = (school) => {
    let newObj = {}  // I create newObj object for adding filter data
    Object.keys(school).forEach((key) => {
        if (school[key].students > 200){     
            newObj[key] = school[key]
        } 
        
    })
    //console.log(newObj)

    
}

filterData(school)



// Question 3 

let addingNewProperty = (school) => {
    const newSchoolData =  JSON.parse(JSON.stringify(school))
    Object.keys(newSchoolData).forEach(val => {
        if (val === "engineering" || val === "medical" || val ===  "pure-science" ){
            newSchoolData[val]["labs-required"] = true
        }else{
            newSchoolData[val]["labs-required"] = false
        }
    })
    return newSchoolData
}

//console.log(addingNewProperty(school))
