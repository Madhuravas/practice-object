const items = {
    item_392019302: {
        name: "Washing Machine",
        stock: 3,
    },
    item_392019342: {
        name: "Light Bulb",
        stock: 3,
    },
    item_392019340: {
        name: "Streaming Device",
        stock: 2
    },
    item_392019389: {
        name: "Plug",
        stock: 1
    },
    item_392019311: {
        name: "Trace",
        stock: 1
    }
}


/*
Q1.Form the following solution

const result = {
    washing_machine: {
        item_id: 'item_392019302',
        stock: 3
    },
    light_bulb: {
        item_id: 'item_392019342',
        stock: 3
    },
    ...
}



Q.2 Write a function that takes items object,  propertyName and value as parameters .
The propertyName is added to each Object.
Try not to mutate the original object.

Q3. Clone items properly 
using object.assign
using spread operator.



*/

// Question 1 

const formDataToNew = (items) => {
    const result = {}
    Object.keys(items).forEach(key =>{
        const eachObj = {}
        eachObj.item_id = key 
        eachObj.stock = items[key].stock
        result[items[key].name] = eachObj
    })
    //console.log(result)

}

formDataToNew(items)

//Question 2 

function addingNewProparty(items,key,value){
    const copyData = JSON.parse(JSON.stringify(items))
    Object.keys(items).forEach((eachItem) =>{
         copyData[eachItem][key] = value
    })
    //console.log(copyData);
}

addingNewProparty(items,"model",2022)

// Question 3

// Object.assign and spread operators are not use for deep cloning so we need to follow JSON.parse(JSON.stringify(items)) method

let cloneUsingAssion = Object.assign({},items) 
let CloneUsingSpread = {...items}
